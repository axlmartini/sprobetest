import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

class EmployeeList extends Component {

    constructor () {
        super()
        this.state = {
          employees: [],
          error: null,
          isLoading: true,
          employeeId: null
        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(id, e){
        e.preventDefault();

        const isDelete = window.confirm("Are you sure you wish to delete this employee?");

        if(isDelete) {
            axios
                .delete(`http://localhost:8000/api/v1/employee/${id}`)
                .then(response => {
                    const { history } = this.props;
                    console.log('Deleted');
                    window.location.reload();
                })
                .catch(error => this.setState({ error }));
        }
    }

    fetchEmployees() {
        axios
            .get("http://localhost:8000/api/v1/employees")
            .then(response => {
                this.setState({
                    employees: response.data.data
                });
            })
            //.then(response => console.log(response.data))
            .catch(error => this.setState({ error }));
    }

    componentDidMount() {
      this.fetchEmployees();
    }

    render() {
        const { employees, error } = this.state;
        
        if(employees.length > 0) {
            return (
                <div>
                    <Link className='btn btn-primary btn-sm mb-3' to='/create'>
                        New Employee
                    </Link>

                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {employees.map(employee => (
                                <tr key={employee.id}>
                                    <td>{employee.id}</td>
                                    <td>{employee.first_name} {employee.last_name}</td>
                                    <td>{employee.email}</td>
                                    <td> <Link to={`/view/${employee.id}`}>View</Link>/ 
                                        <Link to='#' 
                                            onClick={this.handleClick.bind(this, employee.id)}
                                        >Delete</Link>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            );
        } else {
            return (
                <p> Loading.. </p>
            );
        }
    }
}

export default EmployeeList;