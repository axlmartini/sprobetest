import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import withRouter from "react-router";
import { Link } from 'react-router-dom';

class NewEmployee extends Component {
	constructor (props) {
        super(props)
        this.state = {
        	firstName: '',
        	lastName: '',
        	email: '',
        	errors: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleCreateNewEmployee = this.handleCreateNewEmployee.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
 	}

 	handleFieldChange (event) {
        this.setState({
    		[event.target.name]: event.target.value
      	})
	}

	handleCreateNewEmployee (event) {
        event.preventDefault();

        const { history } = this.props;

        const employee = {
          	first_name: this.state.firstName,
          	last_name: this.state.lastName,
          	email: this.state.email
        };

        axios.post('http://localhost:8000/api/v1/employee', employee)
    	    .then(response => {
    		console.log('success');
      	    history.push('/');
    	})
    	.catch(error => {
      	    this.setState({
        		errors: error.response.data.message
            });
        })
    }

    hasErrorFor (field) {
  	    return !!this.state.errors[field]
	}

	renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
        	return (
              	<span className='invalid-feedback d-block'>
                		<strong>{this.state.errors[field][0]}</strong>
              	</span>
        	)
        }
    }

    render () {
        return (
        	<div className='container'>
              	<div className='row justify-content-center'>
            		<div className='col-md-12'>
                        <Link className='btn btn-primary btn-sm mb-3' to='/'>
                            Back
                        </Link>
             			<div className='card'>
                			<div className='card-header'>Create Employee</div>
                			<div className='card-body'>
    		                    <form onSubmit={this.handleCreateNewEmployee}>
    		                      	<div className='form-group'>
    			                        <label htmlFor='firstName'>First name</label>
    			                        <input
    			                          	id = 'firstName'
    			                          	type = 'text'
    			                          	className = {"form-control ${this.hasErrorFor('firstName') ? 'is-invalid' : ''}"}
    			                          	name = 'firstName'
    			                          	value = {this.state.firstName}
    			                          	onChange = {this.handleFieldChange}
    			                        />
    			                        {this.renderErrorFor('first_name')}
    			                    </div>

    			                    <div className='form-group'>
    			                        <label htmlFor='lastName'>Last name</label>
    			                        <input
    			                          	id = 'lastName'
    			                          	type = 'text'
    			                          	className = {"form-control ${this.hasErrorFor('lastName') ? 'is-invalid' : ''}"}
    			                          	name = 'lastName'
    			                          	value = {this.state.lastName}
    			                          	onChange = {this.handleFieldChange}
    			                        />
    			                        {this.renderErrorFor('last_name')}
    			                    </div>

    			                    <div className='form-group'>
    			                        <label htmlFor='name'>Email</label>
    			                        <input
    			                          	id = 'email'
    			                          	type = 'email'
    			                          	className = {"form-control ${this.hasErrorFor('email') ? 'is-invalid' : ''}"}
    			                          	name = 'email'
    			                          	value = {this.state.email}
    			                          	onChange = {this.handleFieldChange}
    			                        />
    			                        {this.renderErrorFor('email')}
    			                    </div>
		                            <button className='btn btn-primary'>Create</button>
    		                    </form>
                			</div>
              		    </div>
            		</div>
              	</div>
        	</div>
        )
    }
}

export default NewEmployee;