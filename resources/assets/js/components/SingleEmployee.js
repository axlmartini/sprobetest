import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Link } from 'react-router-dom';

class SingleEmployee extends Component {

	constructor (props) {
        super(props)
        this.state = {
          	employeeInfo: [],
          	firstName: '',
    		lastName: '',
        	email: '',
        	errors: []
        }

        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleUpdateEmployee = this.handleUpdateEmployee.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)
    }

    fetchEmployee() {

    	const employeeId = this.props.match.params.id;

        axios
            .get(`http://localhost:8000/api/v1/employee/${employeeId}`)
            .then(response => {
                this.setState({
                    employeeInfo: response.data.data
                });
            })
            .catch(error => this.setState({ error }));
    }

    handleFieldChange (event) {
        this.setState({
    		[event.target.name]: event.target.value
      	})
	}

	handleUpdateEmployee (event) {
        event.preventDefault()
       
        const employeeId = this.props.match.params.id;
    	const { history } = this.props

        const employee = {
          	first_name: (this.state.firstName != '') ? this.state.firstName : this.state.employeeInfo.first_name,
          	last_name: (this.state.lastName != '') ? this.state.lastName : this.state.employeeInfo.last_name,
          	email: (this.state.email != '') ? this.state.email : this.state.employeeInfo.email
        }

        axios
        	.put(`http://localhost:8000/api/v1/employee/${employeeId}`, employee)
    	    .then(response => {
	    		console.log('success');
	      	    history.push('/');
	    	})
	    	.catch(error => {
	      	    this.setState({
	        		errors: error.response.data.message
	            });
	        })
    }

    hasErrorFor (field) {
  	    return !!this.state.errors[field]
	}

	renderErrorFor (field) {
        if (this.hasErrorFor(field)) {
        	return (
              	<span className='invalid-feedback d-block'>
                		<strong>{this.state.errors[field][0]}</strong>
              	</span>
        	)
        }
    }

    componentDidMount() {
      this.fetchEmployee();
    }

    render() {
        const { employeeInfo, error } = this.state;
        if(employeeInfo != '') {
        	return (
	            <div>
	                <Link className='btn btn-primary btn-sm mb-3' to='/'>
	                    Back
	                </Link>

	                <div className='card'>
	        			<div className='card-header'>Create Employee</div>
	        			<div className='card-body'>
		                    <form onSubmit={this.handleUpdateEmployee}>
		                      	<div className='form-group'>
			                        <label htmlFor='firstName'>First name</label>
			                        <input
			                          	id = 'firstName'
			                          	type = 'text'
			                          	className = {"form-control ${this.hasErrorFor('firstName') ? 'is-invalid' : ''}"}
			                          	name = 'firstName'
			                          	defaultValue = {employeeInfo.first_name}
			                          	onChange = {this.handleFieldChange}
			                        />
			                        {this.renderErrorFor('first_name')}
			                    </div>

			                    <div className='form-group'>
			                        <label htmlFor='lastName'>Last name</label>
			                        <input
			                          	id = 'lastName'
			                          	type = 'text'
			                          	className = {"form-control ${this.hasErrorFor('lastName') ? 'is-invalid' : ''}"}
			                          	name = 'lastName'
			                          	defaultValue = {employeeInfo.last_name}
			                          	onChange = {this.handleFieldChange}
			                        />
			                        {this.renderErrorFor('last_name')}
			                    </div>

			                    <div className='form-group'>
			                        <label htmlFor='name'>Email</label>
			                        <input
			                          	id = 'email'
			                          	type = 'email'
			                          	className = {"form-control ${this.hasErrorFor('email') ? 'is-invalid' : ''}"}
			                          	name = 'email'
			                          	defaultValue = {employeeInfo.email}
			                          	onChange = {this.handleFieldChange}
			                        />
			                        {this.renderErrorFor('email')}
			                    </div>

	                            <button className='btn btn-primary'>Update</button>
		                    </form>
	        			</div>
	      		    </div>
	            </div>
	        );
    	} else {
    		return (
    			<p>Loading...</p>
			)
    	}
    }
}

export default SingleEmployee;