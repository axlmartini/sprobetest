import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import NewEmployee from './NewEmployee';
import EmployeeList from './EmployeeList';
import SingleEmployee from './SingleEmployee';

class App extends Component {
    render () {
        return (
            <BrowserRouter>
                <div>
                    <Switch>
                        <Route exact path='/' component={EmployeeList} />
                        <Route path='/create' component={NewEmployee} />
                        <Route path='/view/:id' component={SingleEmployee} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));