<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Employee;
use Illuminate\Validation\ValidationException;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\EmployeeCollection;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;

class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * Display a list of all employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return new EmployeeCollection($this->employeeRepository->all());
    }

    /**
     * Display the specific employee.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new EmployeeResource($this->employeeRepository->show($id));
    }

    /**
     * Store a new employee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = $request->only(['first_name', 'last_name', 'email']);

        $validatedData = Validator::make($data, [
            'first_name'  => 'required|max:45',
            'last_name'  => 'required|max:45',
            'email'  => 'required|email|unique:employees,email|max:45'
        ]);

        if($validatedData->fails()) {
            $errors = $validatedData->errors()->toArray();
            return response()->json([ 
                'error' => 400, 
                'message' => $errors 
            ], 500);
        }

        $employee = $this->employeeRepository->store($data);

        return response()->json([
            'success' => true,
            'message' => 'New employee added',
            'data'    => new EmployeeResource($employee)
        ], Response::HTTP_OK);
    }

    /**
     * Store a new employee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request)
    {   
        $employee = Employee::findOrFail($request->id);

        $validatedData = Validator::make($request->all(), [
            'first_name'  => 'required|max:45',
            'last_name'  => 'required|max:45',
            'email'  => 'required|email|unique:employees,email,'.$request->id.'|max:45'
        ]);

        if($validatedData->fails()) {
            $errors = $validatedData->errors()->toArray();
            return response()->json([ 
                'error' => 500, 
                'message' => $errors 
            ], 500);
        }

        $saveEmployee = $this->employeeRepository->update($request->id, $request->all());

        return response()->json([
            'success' => true,
            'message' => 'Employee has been updated',
            'data'    => new EmployeeResource($this->employeeRepository->show($request->id))
        ], Response::HTTP_OK);
    }

    /**
     * Delete a specific employee
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $employee = Employee::findOrFail($request->id);

        $this->employeeRepository->delete($request->id);

        return response()->json([
            'success' => true,
            'message' => 'Employee has been deleted'
        ], Response::HTTP_OK);
    }
}
