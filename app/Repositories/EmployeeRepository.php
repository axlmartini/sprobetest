<?php

namespace App\Repositories;

use App\Employee;
use App\Repositories\Interfaces\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    public function all()
    {
        return Employee::all();
    }

    public function show($id)
    {
    	return Employee::findOrFail($id);
    }

    public function store($data)
    {
    	return Employee::create($data);
    }

    public function update($id, $request)
    {	
    	return Employee::find($id)->update($request);
    }

    public function delete($id)
    {
    	return Employee::find($id)->delete();
    }
    
}