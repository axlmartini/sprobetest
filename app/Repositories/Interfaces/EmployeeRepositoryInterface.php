<?php

namespace App\Repositories\Interfaces;

use App\Employee;

interface EmployeeRepositoryInterface
{
	/**
    * Get's all employees.
    *
    * @return mixed
    */
	public function all();

	/**
    * Get employee by ID.
    *
    * @param int $id
    * @return mixed
    */
	public function show($id);

	/**
    * Insert new Employee.
    *
    * @param array $data
    */
	public function store($data);

	/**
    * Update Employee info.
    *
    * @param int $id
    * @param object $request
    */
	public function update($id, $request);

	/**
    * Delete Employee by id.
    *
    * @param int $id
    */
	public function delete($id);
}