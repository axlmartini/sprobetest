# Installation

## Clone project repository to local

## Project setup

cd to project directory

```
cp .env.example env
update .env file
npm install
composer install
php artisan key:generate
php artisan migrate
php artisan migrate:refresh --seed
npm run watch
php artisan serve
```


## API Requests

### GET
http://localhost:8000/api/v1/employees


### POST
http://localhost:8000/api/v1/employee
{
  "first_name" : "Bingbing",
  "last_name" : "Abing",
  "email" : "bing@gmail.com"
}