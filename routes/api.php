<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware'], function(){
    Route::get('employees', 'EmployeeController@all');
    Route::get('employee/{id}', 'EmployeeController@show');
    Route::post('employee', 'EmployeeController@store');
    Route::put('employee/{id}', 'EmployeeController@update');
    Route::delete('employee/{id}', 'EmployeeController@delete');
});

//fallback route for 404
Route::fallback(function() {
	return response()->json([ 'message' => 'Page not Found' ], 404);
});