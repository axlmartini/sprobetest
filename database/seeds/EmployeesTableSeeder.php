<?php

use Illuminate\Database\Seeder;
use App\Employee;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'first_name' => 'Axell',
            'last_name' => 'Malinao',
            'email' => 'axellmalinao@gmail.com'
        ]);
    }
}
